import java.io.*;
import java.net.*;
import java.util.Scanner;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;

public class manualCalendar
{

/**
 * Manual Calendar manually steps through the Oauth process and 
 * returns events from a calendar.
 * <p>
 * Oauth is a service provided by Google. Most hardcoded variables are
 * retrieved from Google Project services, like clientID and clientSecret.
 * <p>
 * Read the Google API docs for more information.
 * @author      Colleen Blaho
 * @author      Max Mattes
 * @version     %I%, %G%
 * @since       1.0
 */
    public static void main(String[] args) throws Exception {
        String clientID = "1002424366477-bt0e1kaojia5l7jvgogcorcgtma1lmr5.apps.googleusercontent.com";
        String clientSecret = "R9WtDJaNpmF6HJ8lWQx54Wje";
        String APIKey = "AIzaSyDiEvOmQw1E_SVFbWQcH6XC_8K4d3xbwO8";
        String scope = "https://www.googleapis.com/auth/calendar.readonly";
        String redirectURI = "http://cs.drexel.edu";
        String userAgent = "Java/cpb49(at)drexel.edu";
        String calendarAPI = "https://www.googleapis.com/calendar/v3/users/me/calendarList";
        String postParams;
		
		
        String code = recieveCode(scope, redirectURI, clientID);

		String token = recieveToken(code, clientID, clientSecret, redirectURI, userAgent);

		Boolean isValid = validateToken(token, userAgent, clientID);

		if(!isValid) System.exit(-1);

		
        // 1. GET A LIST OF CALENDARS
        URL obj = new URL("https://www.googleapis.com/calendar/v3/users/me/calendarList");
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		postParams = "";
        con.setRequestMethod("GET");
        con.setRequestProperty("Authorization", "Bearer " + token);
       
        JsonObject calendarResponse = getJson(con, postParams, userAgent);

		// 2. GET A LIST OF EVENTS
		String calendarID = calendarResponse.get("items").getAsJsonArray().get(0).getAsJsonObject().get("id").getAsString();
		
		URL calendarEventURL = new URL("https://www.googleapis.com/calendar/v3/calendars/" + calendarID + "/events");
		postParams = "";
		con = (HttpURLConnection) calendarEventURL.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Authorization", "Bearer " + token);        
        JsonObject calendarEvents = getJson(con, postParams, userAgent);
		System.out.println("=====CALENDAR EVENTS=====");
		

		// 3. PRINT THE LIST OF EVENTS

		JsonArray items = calendarEvents.get("items").getAsJsonArray();
		for(int i = 0; i < items.size(); i++) {
   			JsonObject item = items.get(i).getAsJsonObject();
			if (item.has("summary")) {
				System.out.print("\n" + item.get("summary").getAsString());
				JsonObject time = item.get("start").getAsJsonObject();
				if (time.has("date")) {
					System.out.println(" All Day Event: " + time.get("date").getAsString());	
				} else if (time.has("dateTime")) {
					System.out.println(" Date & Time: " + time.get("dateTime").getAsString());
				} else {
					System.out.println(" No time data found");
				}
			}
		}
		
		
    }

	/**
 	* Returns a JsonObject containing the HTTPConnection response.
	* Negotiates HTTPConnection details for a packaged connection.
 	* <p>
 	* Will system.exit() upon 4xx, 5xx, and other server errors.
 	*
 	* @param  con  			a packaged HTTPConnection
 	* @param  postParams 	any parameters that need to be in the POST Body. Blank string for no params.
 	* @param  userAgent		Custom user agent for the program to use
 	* @return      			JsonObject of the server response
	* @see         			Google OAuth Documentation
 	*/
    private static JsonObject getJson(HttpURLConnection con, String postParams, String userAgent) throws Exception {
		con.setRequestProperty("User-Agent", userAgent);
        System.out.println("\n\nSending request to GOOGLE API...");
		if (!postParams.equals("")){
			con.setDoOutput(true);
        	DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        	wr.writeBytes(postParams);
        	wr.flush();
        	wr.close();
		}
        JsonParser jp = new JsonParser();
		JsonElement root;
		try {
        	root = jp.parse(new InputStreamReader((InputStream) con.getContent()));
		} catch (IOException e) {
			System.out.println(e.getMessage());
			root = jp.parse(new InputStreamReader((InputStream) con.getErrorStream()));
			System.out.println(root.toString());
			System.exit(2);
			
		}
		int responseCode = con.getResponseCode();
        System.out.println("Response Code : " + responseCode);
        return root.getAsJsonObject();
    }

	/**
 	* Returns a security code to use to get a token.
	* Accepts user input.
 	* <p>
 	*
 	* @param  scope  		Google scope Specifier
 	* @param  redirectURI 	Redirect URL that matches one in the Google project.
 	* @param  clientID	 	Client ID that matches one in the Google project.
 	* @return      			Authentication code as a String
	* @see         			Google OAuth Documentation
 	*/
	private static String recieveCode(String scope, String redirectURI, String clientID) throws Exception{
		// FIRST STAGE OF OAUTH 2.0 -- REQUESTING A TOKEN CODE
        URL tokenAuth = new URL("https://accounts.google.com/o/oauth2/auth?");
        String postParams = "scope=" + scope + "&" 
                + "state=security_token&"
                + "redirect_uri=" + redirectURI + "&"
                + "response_type=code&" 
                + "client_id=" + clientID + "&"
                + "approval_prompt=auto";
        System.out.println("Paste this into your browser. Give me the token.");
        System.out.println(tokenAuth + postParams); 
        Scanner user_input = new Scanner(System.in);
        return(user_input.next());
		}

	/**
 	* Returns an auth token.
 	* <p>
 	*
 	* @param  code	 		Google authentication code
 	* @param  clientID	 	Client ID that matches one in the Google project.
 	* @param  clientSecret 	Client Secret that matches one in the Google project.
 	* @param  redirectURI 	Redirect URL that matches one in the Google project.
 	* @param  userAgent		Custom user agent for the program to use
 	* @return      			Token as a String
	* @see         			Google OAuth Documentation
 	*/
	private static String recieveToken(String code, String clientID, String clientSecret, String redirectURI, String userAgent) throws Exception{
		// SECOND STAGE OF OAUTH 2.0 -- RECEIVING A TOKEN VIA CODE
		String tokenURL = "https://www.googleapis.com/oauth2/v3/token";
        URL tokenObj = new URL(tokenURL);
        HttpURLConnection con = (HttpURLConnection) tokenObj.openConnection();
        con.setRequestMethod("POST");
        String postParams = "code=" + code +"&" +
            "client_id=" + clientID + "&" +
            "client_secret=" + clientSecret + "&" +
            "redirect_uri=" + redirectURI + "&" +
            "grant_type=authorization_code";
        JsonObject rootobj = getJson(con,postParams, userAgent);
        return(rootobj.get("access_token").getAsString());

		}
	/**
 	* Returns a boolean of the token validity.
 	* <p>
 	*
 	* @param  token	 		Google authentication token
 	* @param  userAgent		Custom user agent for the program to use
 	* @param  clientID	 	Client ID that matches one in the Google project.
 	* @return      			True for valid, false for invalid
	* @see         			Google OAuth Documentation
 	*/
	private static boolean validateToken(String token, String userAgent, String clientID) throws Exception{
		// THIRD STAGE OF OAUTH 2.0 -- VERIFYING A TOKEN
        URL tokenVerify = new URL("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token="+token);
		HttpURLConnection con = (HttpURLConnection) tokenVerify.openConnection();
		String postParams = "";
		JsonObject tokenVerification = getJson(con, postParams, userAgent);
		System.out.println("\nVerifying your identity...");
		String audience = tokenVerification.get("audience").getAsString();

		//mitigate the confused deputy problem
		System.out.print("DONE");
		if (!audience.equals(clientID)){
			return false;
		}
		return true;
	}
		
}
