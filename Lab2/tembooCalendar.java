import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;

import com.temboo.core.TembooSession;
import com.temboo.Library.Google.OAuth.*;
import com.temboo.Library.Google.OAuth.InitializeOAuth.*;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.*;
import com.temboo.Library.Google.Calendar.*;
import com.temboo.Library.Google.Calendar.GetAllCalendars.*;
import com.temboo.Library.Google.Calendar.GetAllEvents.*;

public class tembooCalendar {

/**
 * Temboo Calendar uses Temboo to simplify Oauth and 
 * return events from a calendar.
 * <p>
 * Oauth is a service provided by Google. Most hardcoded variables are
 * retrieved from Google Project services, like clientID and clientSecret.
 * <p>
 * Read the Google API docs and Temboo docs for more information.
 * @author      Colleen Blaho
 * @author      Max Mattes
 * @version     %I%, %G%
 * @since       1.0
 */
    public static void main(String[] args) throws Exception {
        String client_id = "694133753407-lp5io7561t2nti6l3f99u7t4rqfb7q6j.apps.googleusercontent.com";
        String client_secret = "MoONXEjx5UUOz6mo7AA424eX";
        String scope = "https://www.googleapis.com/auth/calendar.readonly";

        TembooSession session = new TembooSession("mm3888", "myFirstApp", "412f3b6e409d4b2b89501ccad596fb1d");
        InitializeOAuthResultSet start = startOAuth(session, client_id, scope);
        
        String url = start.get_AuthorizationURL();
        System.out.println("Please visit this URL and allow access");
        System.out.println(url);

        String access_token = finishOAuth(session, client_id, client_secret, start.get_CallbackID());
        String calendar_json = getCalendars(session, client_id, client_secret, access_token);
        String calendar_id = getFirstId(calendar_json);
        JsonArray events = getEvents(session, client_id, client_secret, access_token, calendar_id);
        print_events(events);
    }

	/**
 	* Prepares and executes the initalizeOAuth Choreo from Temboo.
	*
 	* @param  session		a TembooSession that has been initialized
 	* @param  client_id 	client Id from Google
 	* @param  scope			Google Specificed permission scope
 	* @return      			Choreo response set
	* @see         			Google OAuth Documentation; Temboo Documentation
 	*/
    public static InitializeOAuthResultSet startOAuth(TembooSession session, String client_id, String scope) throws Exception {
        InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

        // Get an InputSet object for the choreo
        InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

        // Set inputs
        initializeOAuthInputs.set_ClientID(client_id);
        initializeOAuthInputs.set_Scope(scope);

        // Execute Choreo
        return initializeOAuthChoreo.execute(initializeOAuthInputs);
    }
	/**
 	* Prepares and executes the finishOAuth Choreo from Temboo.
	*
 	* @param  session		a TembooSession that has been initialized
 	* @param  client_id 	client Id from Google
 	* @param  client_secret	client secret from Google
 	* @param  callback_id	Temboo callback ID
 	* @return      			Oauth token as a string
	* @see         			Google OAuth Documentation; Temboo Documentation
 	*/
    public static String finishOAuth(TembooSession session, String client_id, String client_secret, String callback_id) throws Exception {
        FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

        // Get an InputSet object for the choreo
        FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

        // Set inputs
        finalizeOAuthInputs.set_CallbackID(callback_id);
        finalizeOAuthInputs.set_ClientSecret(client_secret);
        finalizeOAuthInputs.set_ClientID(client_id);

        // Execute Choreo
        return finalizeOAuthChoreo.execute(finalizeOAuthInputs).get_AccessToken();
    }
	/**
 	* Prepares and executes the getAllCalendarsInputs Choreo from Temboo.
	*
 	* @param  session		a TembooSession that has been initialized
 	* @param  client_id 	client Id from Google
 	* @param  client_secret	client secret from Google
 	* @param  access_token	OAuth access Token
 	* @return      			calendar API response as a string
	* @see         			Google OAuth Documentation; Temboo Documentation
 	*/
    public static String getCalendars(TembooSession session, String client_id, String client_secret, String access_token) throws Exception {
        GetAllCalendars getAllCalendarsChoreo = new GetAllCalendars(session);

        // Get an InputSet object for the choreo
        GetAllCalendarsInputSet getAllCalendarsInputs = getAllCalendarsChoreo.newInputSet();

        // Set inputs
        getAllCalendarsInputs.set_ClientID(client_id);
        getAllCalendarsInputs.set_ClientSecret(client_secret);
        getAllCalendarsInputs.set_AccessToken(access_token);

        // Execute Choreo
        return getAllCalendarsChoreo.execute(getAllCalendarsInputs).get_Response();
    }

	/**
 	* Returns the first calendar ID from a Temboo Object
	*
 	* @param  json			json formatted string
 	* @return      			calendar ID as a string
	* @see         			Google OAuth Documentation
 	*/

    public static String getFirstId(String json) {
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(json);
        return root.getAsJsonObject().get("items").getAsJsonArray().get(0).getAsJsonObject().get("id").getAsString();
    }
	/**
 	* Prepares and executes the getAllEventsInputs Choreo from Temboo.
	*
 	* @param  session		a TembooSession that has been initialized
 	* @param  client_id 	client Id from Google
 	* @param  client_secret	client secret from Google
 	* @param  access_token	OAuth access Token
	* @param  calendar_id	Calendar API identifier
 	* @return      			calendar API response as a string
	* @see         			Google OAuth Documentation; Temboo Documentation
 	*/
    public static JsonArray getEvents(TembooSession session, String client_id, String client_secret, String access_token, String calendar_id) throws Exception {
        GetAllEvents getAllEventsChoreo = new GetAllEvents(session);

        // Get an InputSet object for the choreo
        GetAllEventsInputSet getAllEventsInputs = getAllEventsChoreo.newInputSet();

        // Set inputs
        getAllEventsInputs.set_ClientSecret(client_secret);
        getAllEventsInputs.set_CalendarID(calendar_id);
        getAllEventsInputs.set_AccessToken(access_token);
        getAllEventsInputs.set_ClientID(client_id);

        // Execute Choreo
        String event_json = getAllEventsChoreo.execute(getAllEventsInputs).get_Response();
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(event_json);
        return root.getAsJsonObject().get("items").getAsJsonArray();
    }

	/**
 	* Prints calendar events.
	*
 	* @param  events		JsonArray of events from a calendar
 	* @return      			void
	* @see         			Google OAuth Documentation
 	*/

    public static void print_events(JsonArray events) {
        for(int i = 0; i < events.size(); i++) {
            JsonObject event = events.get(i).getAsJsonObject();
            if(event.has("summary")) {
                System.out.println("\n" + event.get("summary").getAsString());
                JsonObject time = event.get("start").getAsJsonObject();
                if (time.has("date")) {
                    System.out.println("All Day Event: " + time.get("date").getAsString()); 
                } else if (time.has("dateTime")) {
                    System.out.println("Date & Time: " + time.get("dateTime").getAsString());
                }
            }
        }
    }
}
