package cblaho.weatherapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by cblaho on 8/13/15.
 */
public class hourlyAdapter extends ArrayAdapter<WeatherHour> {
    public hourlyAdapter(Context context, int resource, WeatherHour[] objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater)
                    this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_layout, parent, false);
        }

        TextView temperature = (TextView) convertView.findViewById(R.id.temperature);
        TextView humidity = (TextView) convertView.findViewById(R.id.humidity);
        TextView description = (TextView) convertView.findViewById(R.id.description);
        TextView dateTime = (TextView) convertView.findViewById(R.id.dateTime);

        temperature.setText(this.getItem(position).getTemperature());
        humidity.setText(this.getItem(position).getHumidity());
        description.setText(this.getItem(position).getDescription());
        dateTime.setText(this.getItem(position).getDateTime());

        new DownloadImage((ImageView) convertView.findViewById(R.id.weatherImage))
                .execute(this.getItem(position).getImageURL());

        return convertView;
    }
}
