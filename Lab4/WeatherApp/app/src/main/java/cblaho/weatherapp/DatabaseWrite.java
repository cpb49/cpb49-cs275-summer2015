package cblaho.weatherapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Created by cblaho on 8/13/15.
 */
public class DatabaseWrite extends AsyncTask<JsonArray, Void, Void> {
    public static final String MY_PREFS_NAME = "WeatherPrefsFile";
    private Context context;
    public DatabaseWrite(Context context){
        this.context = context;
    }
    protected Void doInBackground(JsonArray... object) {

        DatabaseHandler db = new DatabaseHandler(context);
        for (int i = 0; i < object[0].size(); i++) {

            JsonObject forecast = object[0].get(i).getAsJsonObject();

            String description = forecast.get("condition").getAsString();
            String dateTime = forecast.get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
            String temperature = forecast.get("temp").getAsJsonObject().get("english").getAsString() + " degrees F";
            String humidity = forecast.get("humidity").getAsString() + "% humidity";
            String imageURL = forecast.get("icon_url").getAsString();

            WeatherHour hour = new WeatherHour(temperature, humidity, dateTime, description, imageURL);
            db.addHour(hour);
            if (isCancelled()) break;

        }
        return null;
    }

    protected void onPostExecute(JsonObject result) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putLong("last_modified", System.currentTimeMillis());
        editor.apply();
    }
}
