package cblaho.weatherapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cblaho on 8/13/15.
 * Design taken from https://webcache.googleusercontent.com/search?q=cache:RerPuSoAYJoJ:www.androidhive.info/2011/11/android-sqlite-database-tutorial/+&cd=6&hl=en&ct=clnk&gl=us
 */


   public class DatabaseHandler extends SQLiteOpenHelper {

        // All Static variables
        // Database Version
        private static final int DATABASE_VERSION = 1;

        // Database Name
        private static final String DATABASE_NAME = "weatherManager";

        // Weather table name
        private static final String TABLE_WEATHER = "weather";

        // Weather Table Columns name
        private static final String KEY_ID = "id";
        private static final String KEY_TEMPERATURE = "temperature";
        private static final String KEY_DESCRIPTION = "description";
        private static final String KEY_HUMIDITY = "phone_number";
        private static final String KEY_IMAGEURL = "image_url";
        private static final String KEY_DATETIME = "date_time";


        public DatabaseHandler(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        // Creating Tables
        @Override
        public void onCreate(SQLiteDatabase db) {
            String CREATE_WEATHER_TABLE = "CREATE TABLE " + TABLE_WEATHER + "("
                    + KEY_ID + " INTEGER PRIMARY KEY," + KEY_DATETIME + " TEXT,"
                    + KEY_TEMPERATURE + " TEXT," + KEY_DESCRIPTION + " TEXT,"
                    + KEY_HUMIDITY + " TEXT," + KEY_IMAGEURL + "TEXT" + ")";
            db.execSQL(CREATE_WEATHER_TABLE);
        }

        // Upgrading database
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Drop older table if existed
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_WEATHER);

            // Create tables again
            onCreate(db);
        }

        /**
         * All CRUD(Create, Read, Update, Delete) Operations
         */

        // Adding new contact
        void addHour(WeatherHour hour) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_TEMPERATURE, hour.getTemperature());
            values.put(KEY_HUMIDITY, hour.getHumidity());
            values.put(KEY_DATETIME, hour.getDateTime());
            values.put(KEY_DESCRIPTION, hour.getDescription());
            values.put(KEY_IMAGEURL, hour.getImageURL());


            // Inserting Row
            db.insert(TABLE_WEATHER, null, values);
            db.close(); // Closing database connection
        }


        public WeatherHour[] getAllHours() {

            List<WeatherHour> hours = new ArrayList<WeatherHour>();
            String selectQuery = "SELECT  * FROM " + TABLE_WEATHER;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

//            String CREATE_WEATHER_TABLE = "CREATE TABLE " + TABLE_WEATHER + "("
//                    + KEY_ID + " INTEGER PRIMARY KEY," + KEY_DATETIME + " TEXT,"
//                    + KEY_TEMPERATURE + " TEXT " + KEY_DESCRIPTION + " TEXT "
//                    + KEY_HUMIDITY + " TEXT " + KEY_IMAGEURL + "TEXT" + ")";
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    WeatherHour hour = new WeatherHour(cursor.getString(2), cursor.getString(4), cursor.getString(1),cursor.getString(3),cursor.getString(5));
                    hours.add(hour);
                } while (cursor.moveToNext());
            }
            WeatherHour[] result = new WeatherHour[hours.size()];

            return hours.toArray(result);
        }


        public int getHoursCount() {
            String countQuery = "SELECT  * FROM " + TABLE_WEATHER;
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(countQuery, null);
            cursor.close();
            // return count
            return cursor.getCount();
        }

    public void dumpCache(SQLiteDatabase db) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WEATHER);

        // Create tables again
        onCreate(db);

    }
}
