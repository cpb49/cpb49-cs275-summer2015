package cblaho.weatherapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by cblaho on 8/13/15.
 */
public class networkRequest extends AsyncTask<URL, Void, JsonObject> {
    private Context context;
    private ProgressDialog dialog;

    public networkRequest(Context context){
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        dialog = new ProgressDialog(context);
        dialog.setMessage("Please wait...");
        dialog.setIndeterminate(true);
        dialog.show();
        super.onPreExecute();
    }

    protected JsonObject doInBackground(URL... urls) {
        if (isCancelled()) return new JsonObject();
        return getJson(urls[0]);
    }

    public static JsonObject getJson(URL url){
        HttpURLConnection request = null;
        try {
            request = (HttpURLConnection) url.openConnection();
            request.connect();
            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            return root.getAsJsonObject();
        } catch (IOException e){
            System.out.println(e);
            return new JsonObject();
        } finally {
            if (request != null) {
                request.disconnect();
            }
        }
    }

    protected void onPostExecute(JsonObject result) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        fuckingHackyBullshit(result);
    }
    private JsonObject fuckingHackyBullshit(JsonObject myValue) {
        //handle value
        //System.out.println(myValue.get("location").getAsJsonObject().get("zip").getAsString());
        return myValue;
    }
}
