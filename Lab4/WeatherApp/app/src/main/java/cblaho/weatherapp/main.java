package cblaho.weatherapp;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.net.URL;

public class main extends AppCompatActivity {
    String key = "297c795fdddd5321";
    public static final String MY_PREFS_NAME = "WeatherPrefsFile";
    String locationURL = "http://api.wunderground.com/api/" + key + "/geolookup/q/autoip.json";
    String hourlyURL;
    String zipcode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences settings = getSharedPreferences(MY_PREFS_NAME, 0);
        long lastModified = settings.getLong("last_modified", 0);
        //database does not work
        if (true){
            getLocation(locationURL);
            JsonObject hourly = getHourly();
            JsonArray forecasts = hourly.get("hourly_forecast").getAsJsonArray();
            setLayout(forecasts);
        } else {
            DatabaseHandler db = new DatabaseHandler(this);
            final WeatherHour hours[] = db.getAllHours();
            ListView l = (ListView) findViewById(R.id.listView);
            hourlyAdapter a = new hourlyAdapter(this, R.layout.row_layout, hours);
            l.setAdapter(a);

            Toast.makeText(getApplicationContext(), "Using Cached Data From Last Hour",
                    Toast.LENGTH_LONG).show();

            l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(getApplicationContext(), hours[position].getDescription(),
                            Toast.LENGTH_LONG).show();
                }
            });

        }
    }




    private JsonObject getHourly() {
        hourlyURL = "http://api.wunderground.com/api/" + key + "/hourly/q/" + zipcode + ".json";
        URL url = null;
        try {
            url = new URL(hourlyURL);
        } catch (Exception e){
            Log.e("Instantiation", "Learn to copy paste please." + e);
            this.finish();
            System.exit(1);
        }

        AsyncTask hourlyRequest = new networkRequest(this).execute(url);
        JsonObject result = null;
        try {
            result = (JsonObject) hourlyRequest.get();
        } catch (Exception e){
            Log.e("Instantiation", "hourly request failed!" + e);
            this.finish();
            System.exit(1);
        }
        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void onClick(View v) throws Exception{
        return;


    }
    /*protected synchronized GoogleApiClient buildGoogleApiClient() {
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        return mGoogleApiClient;
        */

    private void getLocation(String locationURL){
        URL url = null;
        try {
            url = new URL(locationURL);
        } catch (Exception e){
            Log.e("Instantiation", "Learn to copy paste please. " + e);
            this.finish();
            System.exit(0);
        }
        AsyncTask locationRequest = new networkRequest(this).execute(url);
        JsonObject result = null;
        try {
            result = (JsonObject) locationRequest.get();
            zipcode = result.get("location").getAsJsonObject().get("zip").getAsString();
        } catch (Exception e){
            Log.e("Instantiation", "Location request failed! " + e);
            this.finish();
            System.exit(0);
        }
    }

    public void setLayout(JsonArray forecasts) {
        final WeatherHour hours[] = new WeatherHour[forecasts.size()];
        for (int i = 0; i < forecasts.size(); i++) {

            JsonObject forecast = forecasts.get(i).getAsJsonObject();

            String description = forecast.get("condition").getAsString();
            String dateTime = forecast.get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
            String temperature = forecast.get("temp").getAsJsonObject().get("english").getAsString() + " degrees F";
            String humidity = forecast.get("humidity").getAsString() + "% humidity";
            String imageURL = forecast.get("icon_url").getAsString();

            hours[i] = new WeatherHour(temperature, humidity, dateTime, description, imageURL);
        }

            ListView l = (ListView) findViewById(R.id.listView);
            hourlyAdapter a = new hourlyAdapter(this, R.layout.row_layout, hours);
            l.setAdapter(a);

            l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(getApplicationContext(), hours[position].getDescription(),
                            Toast.LENGTH_LONG).show();
                }
            });



    }

}
