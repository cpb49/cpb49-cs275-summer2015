package cblaho.weatherapp;

/**
 * Created by cblaho on 8/13/15.
 */
public class WeatherHour {
    private String temperature;
    private String humidity;
    private String dateTime;
    private String description;
    private String imageURL;

    public WeatherHour(String _temperature, String _humidity, String _dateTime, String _description, String _imageURL) {
        temperature = _temperature;
        humidity = _humidity;
        dateTime = _dateTime;
        description = _description;
        imageURL = _imageURL;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

}
