package cblaho.zillowplanner;

import java.util.Calendar;

/**
 * Created by cblaho on 8/24/15.
 */
public interface TimePickerFragmentListener {
    public void onTimeSet(int timeID, Calendar time);
}
