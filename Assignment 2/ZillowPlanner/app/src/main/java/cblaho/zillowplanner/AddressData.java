package cblaho.zillowplanner;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by cblaho on 8/24/15.
 */
public class AddressData implements Parcelable {
    public String start;
    public ArrayList<String> addresses;
    public ArrayList<Calendar> startTimes;
    public ArrayList<Calendar> endTimes;
    @Override
    public int describeContents() {
        return 0;
    }

    public void addHouse(String address, Calendar start, Calendar end){
        addresses.add(address);
        startTimes.add(start);
        endTimes.add(end);
    }

    public static final Parcelable.Creator<AddressData> CREATOR = new Parcelable.Creator<AddressData>() {
        public AddressData[] newArray(int size) {return new AddressData[size];}
        public AddressData createFromParcel(Parcel source) {return new AddressData(source);}
    };

    private AddressData(Parcel source){
        start = source.readString();
        addresses = (ArrayList<String>) source.readSerializable();
        startTimes = (ArrayList<Calendar>) source.readSerializable();
        endTimes = (ArrayList<Calendar>) source.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(start);
        dest.writeSerializable(addresses);
        dest.writeSerializable(startTimes);
        dest.writeSerializable(endTimes);
    }

    public AddressData(String start){
        this.start = start;
        addresses = new ArrayList<String>();
        startTimes = new ArrayList<Calendar>();
        endTimes = new ArrayList<Calendar>();
    }

    public int size() {return addresses.size();}
}
