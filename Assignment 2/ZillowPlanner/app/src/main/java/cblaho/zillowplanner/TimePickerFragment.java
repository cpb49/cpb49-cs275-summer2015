package cblaho.zillowplanner;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TimePicker;
import android.text.format.DateFormat;

import java.util.Calendar;

/**
 * Created by cblaho on 8/24/15.
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    private Calendar userSelection;
    private TimePickerFragmentListener listener;
    public int timeID;

    public static TimePickerFragment newInstance(TimePickerFragmentListener listener, int id){
        TimePickerFragment overlay = new TimePickerFragment();
        overlay.setListener(listener);
        overlay.timeID = id;
        return overlay;
    }

    @Override
    public Dialog onCreateDialog(Bundle saved){
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        if(saved != null){
            hour = saved.getInt("hour");
            minute = saved.getInt("minute");
        }
        return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }
    public void setListener(TimePickerFragmentListener listener){
        this.listener = listener;
    }

    protected void notifyListener(Calendar time){
        if(this.listener != null){
            this.listener.onTimeSet(timeID,time);
        }
    }

    public void onSaveInstanceState(Bundle out){
        super.onSaveInstanceState(out);
        out.putInt("hour", userSelection.get(Calendar.HOUR_OF_DAY));
        out.putInt("minute", userSelection.get(Calendar.MINUTE));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        //get today's date
        Calendar current = Calendar.getInstance();
        //overwrite it with user input
        current.set(Calendar.HOUR_OF_DAY, hourOfDay);
        current.set(Calendar.MINUTE, minute);
        userSelection = current;
        notifyListener(userSelection);
    }
}
