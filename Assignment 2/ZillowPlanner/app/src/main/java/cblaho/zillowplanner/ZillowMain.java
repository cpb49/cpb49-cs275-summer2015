package cblaho.zillowplanner;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ZillowMain extends AppCompatActivity  implements TimePickerFragmentListener{
    Map<Integer, Calendar> times = new HashMap<Integer, Calendar>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zillow_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_zillow_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getTime(View view){
        TimePickerFragment fragment = TimePickerFragment.newInstance(this, view.getId());
        fragment.show(getFragmentManager(), "timePicker");
    }


    public void setHouseData(){
        AddressData userInput = new AddressData(((EditText)findViewById(R.id.firstAddress)).getText().toString());
        userInput.addHouse(((EditText) findViewById(R.id.address1)).getText().toString(), times.get(R.id.start1), times.get(R.id.end1));
        userInput.addHouse(((EditText) findViewById(R.id.address2)).getText().toString(), times.get(R.id.start2), times.get(R.id.end2));
        userInput.addHouse(((EditText) findViewById(R.id.address3)).getText().toString(), times.get(R.id.start3), times.get(R.id.end3));
        userInput.addHouse(((EditText) findViewById(R.id.address4)).getText().toString(), times.get(R.id.start4), times.get(R.id.end4));
        userInput.addHouse(((EditText) findViewById(R.id.address5)).getText().toString(), times.get(R.id.start5), times.get(R.id.end5));

        return;
    }

    @Override
    public void onTimeSet(int timeID, Calendar time) {
            times.put(timeID, time); //r.id.FOOBUTTON indexed associative array
            Button button = (Button) findViewById(timeID);
            button.setText(((Integer) time.get(Calendar.HOUR_OF_DAY)).toString() + ":" + ((Integer) time.get(Calendar.MINUTE)).toString());
    }
}
