import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.lang.Math.*;
import java.util.regex.*;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;

import com.temboo.core.*;
import com.temboo.Library.Twitter.OAuth.*;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.*;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.*;
import com.temboo.Library.Twitter.Timelines.*;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.*;


public class TwitterParser {

/**
 * Twitter Parser uses Temboo and Wordnik to 
 * calculate readability statistics for a specified Twitter User.
 * <p>
 * Oauth is a service provided by Twitter. Most hardcoded variables are
 * retrieved from Twitter services, like api_key and api_secret.
 * <p>
 * Read the Twitter API docs and Temboo docs for more information.
 * @author      Colleen Blaho
 * @version     %I%, %G%
 * @since       1.0
 */
    public static void main(String[] args) throws Exception {
	
        String api_key = "WJZlHjTmrcCvFDAutxIi4v1Ac";
        String api_secret = "ud1JSBZqbzet5DQfFsi4Suc7d6hlfFdCDJZjgX8sTzzU93sgwu";
        String access_token;
		String access_token_secret;
		TembooSession session = new TembooSession("cpb49", "myFirstApp", "af1c5dc20c1b4bbdbd7a2719f84cc409");
       	InitializeOAuthResultSet start = startOAuth(session, api_key, api_secret);
		String wordnik_key = "a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5"; 
		int count = 50;
       
       	String url = start.get_AuthorizationURL();
       	System.out.println("Please visit this URL and allow access");
       	System.out.println(url);
       	FinalizeOAuthResultSet token_results  = finishOAuth(session, api_key, api_secret, start.get_CallbackID(), start.get_OAuthTokenSecret());
		access_token = token_results.get_AccessToken();
		access_token_secret = token_results.get_AccessTokenSecret();
		

		System.out.println("Please input the twitter handle you want to analyse (without '@'):");
		Scanner user_input = new Scanner(System.in);
        String screen_name = user_input.next();

		String[] tweets = getTweets(session, api_key, api_secret, access_token, access_token_secret, screen_name, Integer.toString(count));
		int polysyllables = 0;
		int syllables = 0;
		Map<String, Integer> seenWords = new HashMap<String, Integer>();	
		
		for(int i = 0; i < tweets.length; i++){
			System.out.println("Processing Tweet #" + (i+1));
			if(tweets[i] == null)
				continue;
			String[] words = tweets[i].split(" ");
			for(int j = 0; j < words.length; j++){
				words[j] = words[j].replaceAll("\\p{P}", "");
				//check if we've seen this before
				if(seenWords.containsKey(words[j])){
					syllables = seenWords.get(words[j]);
					//System.out.println("cached word: " + words[j]);
				}else{
					syllables = getSyllables(words[j], wordnik_key);
					seenWords.put(words[j], syllables);
				}
				//decide if it's polysyllablic
				if (syllables > 1){
					System.out.println("POLYSYLLABIC WORD: " + words[j] + "," + syllables);
					polysyllables++;
				}
			}
		}
		
		double grade = 1.0430 * Math.sqrt((double)polysyllables * (30 / (double) count)) + 3.1291;
		System.out.println("SMOG Grade Level: " + (int) grade);
		
     
    }

	/**
 	* Prepares and executes the initalizeOAuth Choreo from Temboo.
	*
 	* @param  session		a TembooSession that has been initialized
 	* @param  api_key 		app key from Twitter
	* @param  api_secret	app secret from Twitter
 	* @return      			Choreo response set
	* @see         			Twitter OAuth Documentation; Temboo Documentation
 	*/
    public static InitializeOAuthResultSet startOAuth(TembooSession session, String api_key, String api_secret) throws Exception {
        InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

        // Get an InputSet object for the choreo
        InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

        // Set inputs
        initializeOAuthInputs.set_ConsumerSecret(api_secret);
		initializeOAuthInputs.set_ConsumerKey(api_key);

        // Execute Choreo
        return initializeOAuthChoreo.execute(initializeOAuthInputs);
    }
	/**
 	* Prepares and executes the finishOAuth Choreo from Temboo.
	*
 	* @param  session		a TembooSession that has been initialized
 	* @param  api_key 		client Id from Twitter
 	* @param  api_secret	client secret from Twitter
 	* @param  callback_id	Temboo callback ID
 	* @param  auth_code		authorization code from Twitter
 	* @return      			Oauth token as a string
	* @see         			Twitter OAuth Documentation; Temboo Documentation
 	*/
    public static FinalizeOAuthResultSet finishOAuth(TembooSession session, String api_key, String api_secret, String callback_id, String auth_code) throws Exception {
        FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

        // Get an InputSet object for the choreo
        FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

        // Set inputs
        finalizeOAuthInputs.set_CallbackID(callback_id);
		finalizeOAuthInputs.set_ConsumerSecret(api_secret);
		finalizeOAuthInputs.set_ConsumerKey(api_key);
		finalizeOAuthInputs.set_OAuthTokenSecret(auth_code);
        // Execute Choreo
        return finalizeOAuthChoreo.execute(finalizeOAuthInputs);
    }

	/**
 	* Prepares and executes the UserTimeline Choreo from Temboo.
	*
 	* @param  session		a TembooSession that has been initialized
 	* @param  api_key 		client Id from Twitter
 	* @param  api_secret	client secret from Twitter
	* @param  access_token 		access token from Twitter
 	* @param  access_token_secret	access token secret from Twitter
 	* @param  callback_id	Temboo callback ID
 	* @return      			String[] with tweet text
	* @see         			Twitter OAuth Documentation; Temboo Documentation
 	*/
	public static String[] getTweets(TembooSession session, String api_key, String api_secret, String access_token, String access_token_secret, String screen_name, String count) throws Exception{

		UserTimeline userTimelineChoreo = new UserTimeline(session);

		// Get an InputSet object for the choreo
		UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

		// Set inputs
		userTimelineInputs.set_ExcludeReplies("true");
		userTimelineInputs.set_ScreenName(screen_name);
		userTimelineInputs.set_Count(count);
		userTimelineInputs.set_AccessToken(access_token);
		userTimelineInputs.set_IncludeRetweets("false");
		userTimelineInputs.set_AccessTokenSecret(access_token_secret);
		userTimelineInputs.set_ConsumerSecret(api_secret);
		userTimelineInputs.set_ConsumerKey(api_key);

		UserTimelineResultSet userTimelineResults = null;
		// Execute Choreo
		try{
		userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);
		} catch (TembooException e){
			System.out.println(e.toString());
			System.out.println("\n\nPlease attempt running this program again. Check to make sure the Twitter Handle is valid.");
			System.exit(1);
		}

		JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(userTimelineResults.get_Response());
		JsonArray tweets = root.getAsJsonArray();
		String[] returnArray = new String[Integer.parseInt(count)];

		for(int i = 0; i < tweets.size(); i++) {
   			JsonObject item = tweets.get(i).getAsJsonObject();
			returnArray[i] = item.get("text").getAsString();
		}
		return returnArray;

	}

	/**
 	* 
	* Negotiates HTTPConnection details for a connection.
	* Returns an integer, the number of syllables Wordnik reports
 	* <p>
 	* Will system.exit() upon 4xx, 5xx and other server errors. (Ignores 401.)
 	*
 	* @param  word 			the target word
 	* @param  wordnik_key 	Wordnik API key
 	* @param  userAgent		Custom user agent for the program to use
 	* @return      			JsonObject of the server response
	* @see         			Wordnik Documentation
 	*/
    private static int getSyllables(String word, String wordnik_key) throws Exception {
		if (word.length() < 1) return 0;
		
		String tokenURL = "http://api.wordnik.com:80/v4/word.json/" + URLEncoder.encode(word, "UTF-8") + "/hyphenation?useCanonical=false&limit=50&api_key=" + wordnik_key;
        URL tokenObj = new URL(tokenURL);
        HttpURLConnection con = (HttpURLConnection) tokenObj.openConnection();
        con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", "cpb49(at)drexel.edu/JAVA");
        //System.out.println("\n\nSending request to WORDNIK API...");
		//if (!postParams.equals("")){
		//	con.setDoOutput(true);
        //	DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        //	wr.writeBytes(postParams);
        //	wr.flush();
        //	wr.close();
		//}
        JsonParser jp = new JsonParser();
		JsonElement root;
		try {
        	root = jp.parse(new InputStreamReader((InputStream) con.getContent()));
		} catch (IOException e) {
			if(con.getResponseCode() == 401){
				//System.out.println("Response Code : " + con.getResponseCode());
				//System.out.println("invalid characters, does not count as a word");
				return 0;
			}
			System.out.println(e.getMessage());
			root = jp.parse(new InputStreamReader((InputStream) con.getErrorStream()));
			System.out.println(root.toString());
			System.exit(2);
		}
		//int responseCode = con.getResponseCode();
        //System.out.println("Response Code : " + responseCode);
		
		//if the word does not exist, it returns a 0 array with a status code of 200
        return root.getAsJsonArray().size();
    }

}
