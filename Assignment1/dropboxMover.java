import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.regex.*;


import org.apache.commons.codec.binary.Base64;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;

import com.temboo.core.TembooSession;
import com.temboo.Library.Dropbox.OAuth.*;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.*;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.*;
import com.temboo.Library.Dropbox.FileOperations.*;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.*;
import com.temboo.Library.Dropbox.FilesAndMetadata.*;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.*;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile.*;


public class dropboxMover {

/**
 * Dropbox Mover uses Temboo to simplify Oauth and 
 * move files around on Dropbox.
 * <p>
 * Oauth is a service provided by Dropbox. Most hardcoded variables are
 * retrieved from Dropbox services, like app_key and app_secret.
 * <p>
 * Read the Dropbox API docs and Temboo docs for more information.
 * @author      Colleen Blaho
 * @version     %I%, %G%
 * @since       1.0
 */
    public static void main(String[] args) throws Exception {
        String app_key = "e3bvf7c1pqlz19p";
        String app_secret = "iflrht2noxxwhoh";
        String access_token;
		String access_token_secret;
		int user_id;
		TembooSession session = new TembooSession("cpb49", "myFirstApp", "af1c5dc20c1b4bbdbd7a2719f84cc409");
		Base64 decoder = new Base64();
		
		System.out.println("Cached Credentials not found. Using Temboo for authentication.");
       	InitializeOAuthResultSet start = startOAuth(session, app_key, app_secret);
       
       	String url = start.get_AuthorizationURL();
       	System.out.println("Please visit this URL and allow access");
       	System.out.println(url);
       	FinalizeOAuthResultSet token_results  = finishOAuth(session, app_key, app_secret, start.get_CallbackID(), start.get_OAuthTokenSecret());
		access_token = token_results.get_AccessToken();
		access_token_secret = token_results.get_AccessTokenSecret();
		user_id = Integer.parseInt(token_results.get_UserID());	

		
        //get __list file
		//System.out.println(access_token + " " + access_token_secret + " " + user_id);
		String list_contents = getFile(session, access_token, access_token_secret, app_key, app_secret, "move/__list");
		parseList(list_contents, session, access_token, access_token_secret, app_key, app_secret);
		resetList(session, access_token, access_token_secret, app_key, app_secret);
		
        
    }

	/**
 	* Prepares and executes the initalizeOAuth Choreo from Temboo.
	*
 	* @param  session		a TembooSession that has been initialized
 	* @param  app_key 		app key from Dropbox
 	* @return      			Choreo response set
	* @see         			Dropbox OAuth Documentation; Temboo Documentation
 	*/
    public static InitializeOAuthResultSet startOAuth(TembooSession session, String app_key, String app_secret) throws Exception {
        InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

        // Get an InputSet object for the choreo
        InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

        // Set inputs
        initializeOAuthInputs.set_DropboxAppSecret(app_secret);
		initializeOAuthInputs.set_DropboxAppKey(app_key);

        // Execute Choreo
        return initializeOAuthChoreo.execute(initializeOAuthInputs);
    }
	/**
 	* Prepares and executes the finishOAuth Choreo from Temboo.
	*
 	* @param  session		a TembooSession that has been initialized
 	* @param  app_key 	client Id from Dropbox
 	* @param  app_secret	client secret from Dropbox
 	* @param  callback_id	Temboo callback ID
 	* @return      			Oauth token as a string
	* @see         			Dropbox OAuth Documentation; Temboo Documentation
 	*/
    public static FinalizeOAuthResultSet finishOAuth(TembooSession session, String app_key, String app_secret, String callback_id, String auth_code) throws Exception {
        FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

        // Get an InputSet object for the choreo
        FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

        // Set inputs
        finalizeOAuthInputs.set_CallbackID(callback_id);
		finalizeOAuthInputs.set_DropboxAppSecret(app_secret);
		finalizeOAuthInputs.set_DropboxAppKey(app_key);
		finalizeOAuthInputs.set_OAuthTokenSecret(auth_code);
        // Execute Choreo
        return finalizeOAuthChoreo.execute(finalizeOAuthInputs);
    }

	/**
 	* Prepares and executes the getFile Choreo from Temboo.
	*
 	* @param  session		a TembooSession that has been initialized
 	* @param  app_key 		client Id from Dropbox
 	* @param  app_secret	client secret from Dropbox
	* @param  access_token 		access token from Dropbox
 	* @param  access_token_secret	access token secret from Dropbox
 	* @param  callback_id	Temboo callback ID
 	* @return      			Base64 file contents as a string
	* @see         			Dropbox OAuth Documentation; Temboo Documentation
 	*/
	
	public static String getFile(TembooSession session, String access_token, String access_token_secret, String app_key, String app_secret, String file_path) throws Exception{
		GetFile getFileChoreo = new GetFile(session);

		// Get an InputSet object for the choreo
		GetFileInputSet getFileInputs = getFileChoreo.newInputSet();

		// Set inputs
		getFileInputs.set_AccessToken(access_token);
		getFileInputs.set_AppSecret(app_secret);
		getFileInputs.set_AccessTokenSecret(access_token_secret);
		getFileInputs.set_AppKey(app_key);
		getFileInputs.set_Path(file_path);

		// Execute Choreo
		GetFileResultSet getFileResults = getFileChoreo.execute(getFileInputs);	
		return getFileResults.get_Response();
	}

	/**
 	* Prepares and executes the DeleteFileOrFolder Choreo from Temboo, with a hardcoded target of "/move/__list".
	* Consider the hardcoding an insurance policy against my own stupidity.
	*
 	* @param  session		a TembooSession that has been initialized
 	* @param  app_key 		client Id from Dropbox
 	* @param  app_secret	client secret from Dropbox
	* @param  access_token 		access token from Dropbox
 	* @param  access_token_secret	access token secret from Dropbox
 	* @return      			void
	* @see         			Dropbox OAuth Documentation; Temboo Documentation
 	*/

	public static void resetList(TembooSession session, String access_token, String access_token_secret, String app_key, String app_secret) throws Exception {

		DeleteFileOrFolder deleteFileOrFolderChoreo = new DeleteFileOrFolder(session);

		// Get an InputSet object for the choreo
		DeleteFileOrFolderInputSet deleteFileOrFolderInputs = deleteFileOrFolderChoreo.newInputSet();

		// Set inputs
		deleteFileOrFolderInputs.set_AppSecret(app_secret);
		deleteFileOrFolderInputs.set_AccessToken(access_token);
		deleteFileOrFolderInputs.set_AccessTokenSecret(access_token_secret);
		deleteFileOrFolderInputs.set_AppKey(app_key);
		deleteFileOrFolderInputs.set_Path("/move/__list");

		// Execute Choreo
		DeleteFileOrFolderResultSet deleteFileOrFolderResults = deleteFileOrFolderChoreo.execute(deleteFileOrFolderInputs);

	}


	/**
 	* Parses the __list file and saves the specified files to their destinations.
	*
 	* @param  session		a TembooSession that has been initialized
 	* @param  app_key 		client Id from Dropbox
 	* @param  app_secret	client secret from Dropbox
	* @param  access_token 		access token from Dropbox
 	* @param  access_token_secret	access token secret from Dropbox
 	* @return      			void
	* @see         			Dropbox OAuth Documentation; Temboo Documentation
 	*/
	public static void parseList(String list_contents, TembooSession session, String access_token, String access_token_secret, String app_key, String app_secret) throws Exception{
		Base64 decoder = new Base64();
		byte[] decodedBytes = decoder.decode(list_contents);
		//System.out.print(new String(decodedBytes).replace("\n", "\\n"));

		String[] parts = (new String(decodedBytes)).split("\n");
		// regex for filename/filepath: (^[\w,\s-]+\.[A-Za-z]*?) (.*)
		// unix is really flexible and I have seen some weird dirnames in my time
		Pattern pattern = Pattern.compile("(^[\\w,\\s-]+\\.[A-Za-z]*?) (.*)");
		Matcher matcher = pattern.matcher("");
		
		for (int i = 0; i < parts.length; i++){
			if (parts[i].length() > 1){
				matcher = pattern.matcher(parts[i]);
				matcher.find();
    			String targetFileName = matcher.group(1);
				System.out.println(targetFileName);
				String targetFileBytes = getFile(session, access_token, access_token_secret, app_key, app_secret, "/move/" + targetFileName);
				byte[] targetFileContents = decoder.decode(targetFileBytes);
    			String targetFolder = matcher.group(2);
				System.out.println(targetFolder);
				FileOutputStream outputStream = new FileOutputStream(targetFolder + targetFileName);            
				outputStream.write(targetFileContents);
				outputStream.flush();
				outputStream.close();
			}
		}
	}
}
