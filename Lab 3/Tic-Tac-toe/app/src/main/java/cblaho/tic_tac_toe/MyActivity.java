package cblaho.tic_tac_toe;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class MyActivity extends ActionBarActivity {
    private boolean playerTurn = false; // Who's turn is it? false=P1x true=P2o
    private int board[][] = new int[3][3]; // P1x = 1. P2o = 2.
    private int moves = 0; // how many moves have we done
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void newGame(View view) {
        playerTurn = false;
        board = new int[3][3]; //default for primitives is 0
        resetButtons();
    }

    private void resetButtons() {
        TableLayout T = (TableLayout) findViewById(R.id.tableLayout);
        for (int y = 0; y < T.getChildCount(); y++) {
            if (T.getChildAt(y) instanceof TableRow) {
                TableRow R = (TableRow) T.getChildAt(y);
                for (int x = 0; x < R.getChildCount(); x++) {
                    if (R.getChildAt(x) instanceof Button) {
                        Button B = (Button) R.getChildAt(x);
                        B.setText("");
                        B.setEnabled(true);
                    }
                }
            }
        }
        TextView t = (TextView) findViewById(R.id.titleText);
        t.setTextColor(Color.rgb(0,0,0));
        t.setText("Player 1's Turn");
        moves = 0;
    }

    public void placeXO(View view) {
        Button b = (Button)view;
        String buttonText = b.getText().toString();

        if(!buttonText.isEmpty()){
            return; //the button is not a valid target
        }

        buttonMark(b);

        int winner = getWinner(board, moves);
        System.out.println(winner);
        TextView t = (TextView) findViewById(R.id.titleText);
        switch (winner){
            case 0: break;
            case 1: t.setTextColor(Color.rgb(200,0,0));
                    t.setText("PLAYER 1 WINS");
                    return;
            case 2: t.setTextColor(Color.rgb(200,0,0));
                    t.setText("PLAYER 2 WINS");
                    return;
            case 3: t.setTextColor(Color.rgb(200,0,0));
                    t.setText("DRAW");
                    return;
        }

        if (playerTurn) {
            t.setText("Player 2's Turn");
        } else {
            t.setText("Player 1's Turn");
        }
    }


    private int getWinner(int[][] board, int moves){
        int returnval = 0; //assume not a match
        boolean h;
        boolean v;
        if (moves == 9){
            System.out.println(returnval);
            returnval = 3; //draw possible
        }

        //check the diagonals manually since they require a specific configuration
        h = board[0][0] == (board[1][1]) && board[0][0]== (board[2][2]) && board[0][0] != 0;
        if(h){
            return board[0][0]; //this should be set to the player id
        }
        v = board[2][0]== (board[1][1]) && board[2][0]== (board[0][2]) && board[0][2] != 0;
        if (v){
            return board[2][0]; //this should be set to the player id
        }

        //loop through rows and columns to save repetition
        for(int i=0; i<3; i++) {
            h = board[i][0] == (board[i][1]) && board[i][0]== (board[i][2]) && board[i][0] != 0;
            if(h){
                return board[i][0]; //this should be set to the player id by click listener
            }
            v = board[0][i]== (board[1][i]) && board[0][i]== (board[2][i]) && board[0][i] != 0;
            if (v){
                return board[0][i]; //this should be set to the player id by click listener
            }
        }
        //if we've gotten here it's either a fail or a draw
        return returnval;

    }

    private void buttonMark(Button b){
        String id = (String) b.getTag();
        int row = Integer.parseInt(id.substring(0,1));
        int col = Integer.parseInt(id.substring(1,2));

        if (playerTurn){
            board[row][col] = 2;
            b.setText("O");
        } else {
            board[row][col] = 1;
            b.setText("X");
        }
        moves++;
        playerTurn = !playerTurn;
    }
}
